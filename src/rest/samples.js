// In the rest operator everything becomes an array!
// If you receive an array, the array will be position 0 in a parent array
// Take a look in each execution below

/** Receive arguments with rest operator and shows in console */
const handle = (...arguments) => console.log(arguments);

// Arguments
handle(1,2,3);
// Array as 1 argument
handle([1,2,3,4,5,6]);
// 2 array arguments
handle(
	[1,2,3],
	[4,5,6]
);
// 2 object arguments
handle(
	{name: 'Adrian', age: 19},
	{name: 'Joseph', age: 22}
);

const variable = 123;

// Traditional template string nesting variable inside it's structure
console.log(`Template string showing variable ${variable}`);

// Template string multinline and processing operations directly
console.log(`
	Multiline template string
	Every tab, space or blank line will be rendered
	Feel free using this awesome feature
	The variable value + 100 directly is: ${variable + 100}
`);

// Official example in tagging template strings
const a = 5;
const b = 10;

const tag = (strings, ...values) => {
  console.log(strings[0]); // "Hello "
  console.log(strings[1]); // " world"
  console.log(values[0]);  // 15
  console.log(values[1]);  // 50

  return "Bazinga!";
}

tag`Hello ${ a + b } world ${ a * b}`;

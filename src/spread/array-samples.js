const array1 = [1, 5, 8, 10];
const array2 = [2, 4, 9, 11];

// New array composed by 2 arrays with their whole positions at the same level
// The attribution respects the order you set
const array3 = [...array1, ...array2];
console.log('New array from merge of array1 and array2', array3);

// Inverting the order in composition
const invertedArray = [...array2, ...array1];
console.log('Array composed in a different order', invertedArray);

// Another approach is to build a new structure nesting objects with spread operator and different elements
const nested = [99999, ...array1, new Date(), ...array2, ...array3, 'A', ...invertedArray];
console.log('New array composed by spread operators and other different elements - Converted into string:\n', nested.join(', '));

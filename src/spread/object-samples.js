const person = {name: "Rodrigo", age: "28"};
const address = {country: "Brazil", state: "São Paulo"};

// New object composed by 2 objects with their whole positions at the same level
// The attribution respects the order you set
const person_with_address = {...person, ...address};
console.log('Object from merging 2 objects with spread operator', person_with_address);

// Inverting the order in composition
const invertedObject = {...person, ...address};
console.log('Object composed in a different order', invertedObject);

// Another approach is to build a new structure nesting objects with spread operator and different elements
const nested = {number: 120, string: 'A', ...person, date: new Date(), ...address};
console.log('New object composed by spread operators and other different elements', nested);

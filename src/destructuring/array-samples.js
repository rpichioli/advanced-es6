const array = [
	{id: 0, name: 'Juliette'},
	{id: 1, name: 'Mark'},
	{id: 2, name: 'Annelise'},
	{id: 3, name: 'Santiago'},
	{id: 4, name: 'Evair'}
]

// Destructuring objects directly from array
// Objects will be according to it's original positions
// Variable names are free, instead of object destructuring that must match with the attributes you must get
const [obj1, obj2, obj3] = array;
console.log('Destrutured objects from array\n', obj1, '\n', obj2, '\n', obj3);

// We can get destructure parcially using rest
// Variables will what is stored in that specific position
// Rest will get the whole other part of the array in an array structure
const [anotherObj1, anotherObj2, ...others] = array;
console.log(`Destrutured 2 array positions and others under rest operator\nPosition 0:\n`, anotherObj1, '\nPosition 1:\n', anotherObj2, '\nOthers:\n', others);

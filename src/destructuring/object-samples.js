const object = {id: 0, name: 'Juliette', age: 30, job: 'Secretary'};

// Extract object attributes by their exact names
const {id, name, age, job} = object;
console.log(`Destrutured all fields individually - id: ${id}, name: ${name}, age: ${age}, job: ${job}`);

// Another way to use is to get other attributes in an isolated collection using rest operator
// We are renaming variables in destructurin because the originals are already being used
// The variables in concept must have the same name so we tell the attributes the 'new' variables they will go to, mapping them
const {id: id_2, name: name_2, ...attributes} = object;
console.log(`Destrutured 2 fields and others under rest operator - id: ${id_2}, name: ${name_2}, others:`, attributes);

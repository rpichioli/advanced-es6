const fetch = require("node-fetch");

const api_base_url = 'https://jsonplaceholder.typicode.com/';

const groupCommentIntoPosts = async (collection) => {
	console.log('-- entered async function --');

	// Wait sync execution from every promise returning new object merged from original and the comments
	let grouped_collection = await Promise.all(collection.map(async item => {
		const response = await fetch(`${api_base_url}comments?postId=${item.id}`);
		const comments = await response.json();
		return Object.assign(item, { comments });
	}));

	console.log('-- finished async function --');
	return await grouped_collection;
}

function* generator() {
	console.log('-- entered generator --');

	// Pending promise
	const response = yield fetch(`${api_base_url}posts`);
	// JSON promise
	const posts = yield response.json();
	// Grouped data from async/await function in a promise
	yield groupCommentIntoPosts(posts);
}

function coroutine(fn) {
	// Instantiate generator
	const generator = fn();
	// Collect fetch promise
	const posts_promise = generator.next().value;
	// Initiating return collection variable
	let posts_with_comments;

	// Intercept promise successful execution
	posts_promise.then(response => {

		// Collect JSON promise
		let json_promise = generator.next(response).value;
		// Collect data from JSON promise
		json_promise.then(posts => {
			// Collect grouped data from generator
			generator.next(posts).value.then(data => console.log(data));
		});
	});
}

console.log('Starting execution..');

coroutine(generator);

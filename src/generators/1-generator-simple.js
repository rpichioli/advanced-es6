/** The generator! */
function* generator() {
	console.log('GENERATOR', 'first step..');
	let value = 1;
	console.log('GENERATOR', 'next:', value);
	value = yield `${value} a`;
	console.log('GENERATOR', 'next:', value);
	value = yield `${value} b`;
	console.log('GENERATOR', 'next:', value);
	value = yield `${value} c`;
	console.log('GENERATOR', 'next:', value);
}

/**
 * Output for every generator iteration received
 * @param  {object} iteration Generator iteration
 * @return {boolean} Identify if generator is finished or not
 */
const output = (iteration) => {
	console.log('OUTPUT', 'done:', iteration.done, 'yield returned value:', iteration.value)
	return iteration.done;
};

const iterator = generator();

let ready	= false;
let value = 1;
while (!ready) {
	ready = output(iterator.next(value));
	value++;
}

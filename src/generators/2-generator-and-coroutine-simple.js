// Coroutine wrapper default 3 steps
// 1 - Call the function and store the resulting coroutine object.
// 2 - Call next() on the coroutine object, to execute until the first yield.
// 3 - After that all you can do is call next(...) to run the coroutine and send it values.

/** Generator providing logic by each iteration */
function* generator() {
	let state = 0;
	yield state++;
	yield state++;
	yield state++;
	yield state++;
	yield state++;
	yield state++;
	yield state++;
}

/** Coroutine receiving and controlling generator's iterations and working in a synchronous stateful runtime */
const coroutine = (fn) => {
	// Storing generator
	const gen = fn();

	// Created recursive function to control generator
	let doNext = () => {
		// Call next iteration
		let iteration = gen.next();
		// Console feedback containing iteration
		console.log(`Value: ${iteration.value} - Done: ${iteration.done}`);
		// Call next iteration if it's not done, recursivelly
		if (!iteration.done) doNext();
	};

	// Initialize the generator controller function
	doNext();
}

// Executing the generator
coroutine(generator);

// Final console feedback
console.log(`Routine has finished!`);

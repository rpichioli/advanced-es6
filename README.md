# advanced-es6
>In development stage

## Motivation
Share with developers most of ES6 advanced features, how to use them, understand the concepts under the hood of each one and close-to-life samples.

## Recommendations
I strongly recommend you to use Node.js to run the js files.

If you don't know how to install it proceed to the next topic called **Installing Node.js**.

Assuming you have Node.js installed and working properly, you just need to enter terminal and execute this way:
```
node file_you_want_to_execute.js
```

It's that!

## Installing Node.js
You can easily download in the [official website](https://nodejs.org/en/download/).

After the installation we need to test if everything is working properly.

Open the terminal and execute:

```
node -v
```

This way you'll get node's respective version if the installation was fully OK.

The next step is to execute other command line:

```
npm -v
```

This way you test the npm, the same way you test node.

If you have both messages you are ready to go! :)

## Structure
Whole developed files are inside `src/` folder - the application folder.

There are many folders inside `src/` representing ES6 points of interest.

The folders are grouping the js files in a 'categorized' way in an atempt to make isolated samples and to become easy to find.

The js files are inside the folders under `src/`.

## Author
Developed by Rodrigo Quiñones Pichioli, [rpichioli](https://github.com/rpichioli).

Feel free to contribute or suggest new stuff! :)
